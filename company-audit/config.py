import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))

HOST = os.environ.get('HOST', '127.0.0.1')
PORT = os.environ.get('PORT', 8080)

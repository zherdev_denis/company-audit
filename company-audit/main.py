from aiohttp import web

import config
from routes import setup_routes

app = web.Application()
setup_routes(app)
web.run_app(app, host=config.HOST, port=config.PORT)
